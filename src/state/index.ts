import {applyMiddleware, createStore} from "redux";
import {rootReducer} from "./reducers";
//import AsyncStorage from '@react-native-async-storage/async-storage';
//import { persistStore, persistReducer } from 'redux-persist'
import thunk from "redux-thunk";

// const persistConfig: any = {
//   key: "root",
//   storage: AsyncStorage,
// };
//
// const persistedReducer = persistReducer(persistConfig, rootReducer)
//
// export default (): any => {
//   const store = createStore( persistedReducer, applyMiddleware(thunk) );
//   const persistor = persistStore( store );
//   return {store, persistor}
// }
export const store = createStore(rootReducer, applyMiddleware(thunk));