import {ChatTypes, QueueTypes, ThemeTypes} from "../actions-types";

interface IFetchThemeAction {
  type: ThemeTypes.FETCH_THEMES;
}
interface ISuccessThemeAction {
  type: ThemeTypes.FETCH_THEMES_SUCCESS;
  payload: any[]
}
interface ISubThemesAction {
  type: ThemeTypes.SUBTHEME;
  payload: string;
}
interface IGetThemeId {
  type: ThemeTypes.THEME_ID;
  payload: number
}

interface IFetchQueueAction {
  type: QueueTypes.FETCH_QUEUE;
}
interface IGetQueueAction {
  type: QueueTypes.FETCH_QUEUE_SUCCESS;
  payload: number
}

interface ISignInChat {
  type: ChatTypes.SIGN_IN_CHAT;
  payload: {
    name: string,
    initialMessage: string,
  }
}
interface IWaitChat {
  type: ChatTypes.WAITING_START_CHAT;
  payload: {
    isQueue: boolean,
    dialogId: string,
  }
}
interface IGetMessages {
  type: ChatTypes.GET_MESSAGES;
}
interface IGetMessagesSuccess {
  type: ChatTypes.GET_MESSAGES_SUCCESS;
  payload: {
    message: any[]
    messageKey: string[]
  }
}
interface ISendMessage {
  type: ChatTypes.SEND_MESSAGES;
}

export type Action = IFetchThemeAction | ISuccessThemeAction | ISubThemesAction | IGetThemeId;
export type QueueAction = IFetchQueueAction | IGetQueueAction;
export type ChatAction = ISignInChat | IWaitChat | IGetMessages | IGetMessagesSuccess | ISendMessage