import database from '@react-native-firebase/database';
import {Dispatch} from "redux";
import {Action, ChatAction, QueueAction} from "../action";
import {ChatTypes, QueueTypes, ThemeTypes} from "../actions-types";
import AsyncStorage from "@react-native-async-storage/async-storage"
import {Actions} from "react-native-router-flux";


export const fetchThemes = () => {
  return async (dispatch: Dispatch<Action>): Promise<void> => {
    try{
      dispatch({type: ThemeTypes.FETCH_THEMES});
      const response: any = await database()
        .ref('/themes')
        .once('value')
        .then((snapshot) => (
          snapshot.val()
        ));
      console.log("all themes====>",response);
      dispatch({
        type: ThemeTypes.FETCH_THEMES_SUCCESS,
        payload: response
      })
    }
    catch (error) {
      console.log(error)
    }
  }
}

export const getThemeId = (id: number): Action => {
  return {
    type: ThemeTypes.THEME_ID,
    payload: id
  }
}

export const getSubTheme = (subTheme: string): Action => {
  return {
    type: ThemeTypes.SUBTHEME,
    payload: subTheme
  }
}

export const fetchQueue = (dialogId: string) => {
  return async (dispatch: Dispatch<QueueAction>): Promise<void> => {
    try {
      dispatch({type: QueueTypes.FETCH_QUEUE});
      await database()
        .ref("/dialogs")
        .orderByChild("/uid")
        .equalTo("new")
        .on("value", (snapshot) => {
          console.log(snapshot.val())
          if(snapshot.val()){
            const queue = Object.keys(snapshot.val()).reverse();
            dispatch({
              type: QueueTypes.FETCH_QUEUE_SUCCESS,
              payload: queue.indexOf(dialogId)+1
            })
          }
        })
      console.log(dialogId)
      database().ref(`/dialogs/${dialogId}/`).on("value",async (snapshot) => {
          const chatStarted = await snapshot.val();
          if(chatStarted.chatStarted) {
            setCash(["isQueue", "false"], ["chatStarted", "true"]);
            Actions.push("chatPage");
          }
          console.log("000",chatStarted.chatStarted);
        })
    }
    catch (error) {
      console.log(error)
    }
  }
}


export const signInChat = (data: { name: string, initialMessage: string }) => {
  return async (dispatch: Dispatch<ChatAction>): Promise<void> => {
    try {
      dispatch({
        type: ChatTypes.SIGN_IN_CHAT,
        payload:  data
      });
      const newData = {
        isQueue : true,
        chatStarted: false,
        name : data.name,
        uid : "new",
        messages: []
      }
      const dialogId = await database().ref("/dialogs").push(newData).key as string;
      database().ref(`/dialogs/${dialogId}/messages`).push({
        content : data.initialMessage,
        timestamp : Date.now(),
        writtenBy : "client"
      })
      dispatch({
        type: ChatTypes.WAITING_START_CHAT,
        payload: {
          dialogId: dialogId,
          isQueue: true
        }
      })
      setCash(["isQueue", "true"], ["dialogId", dialogId]);
    }
    catch (error) {
      console.log(error)
    }
  }
}

const setCash = async (firstPair: string[], secondPair: string[]): Promise<void> => {
  try {
    await AsyncStorage.multiSet([firstPair, secondPair])
  } catch(e) {
    //save error
  }
  console.log('Done.')
}


export const fetchMessages = (dialogId: string) => {

  return async (dispatch: Dispatch<ChatAction>): Promise<void> => {
    try{
      console.log(dialogId, "ID???")
      dispatch({
        type: ChatTypes.GET_MESSAGES
      });
      await database().ref(`/dialogs/${dialogId}/messages`).orderByValue()
        .on("value", (snapshot) => {
        console.log("asdhaksdhjaskldjhkasdhalsk", snapshot.val())
        dispatch({
          type: ChatTypes.GET_MESSAGES_SUCCESS,
          payload: {
            message: Object.values(snapshot.val()).reverse(),
            messageKey: Object.keys(snapshot.val()).reverse()
          }
        })
      })
    }
    catch (err) {
      console.log(err)
    }
  }
}

export const sendMessage = (message: string, dialogId: string) => {
  return (dispatch: Dispatch<ChatAction>): void => {
    try{
      database().ref(`/dialogs/${dialogId}/messages`).push({
        content: message,
        timestamp: Date.now(),
        writtenBy: "client"
      })
    }
    catch (error) {

    }
  }
}