import {Action} from "../action";
import {ThemeTypes} from "../actions-types";


export interface IState {
  themes: any[];
  subTheme: string;
  themeId: number;
}

const initialState: IState = {
  themes: [],
  subTheme: "",
  themeId: 999
};



export const ThemeReducer = (state = initialState, action: Action): IState => {
  switch (action.type) {
    case ThemeTypes.FETCH_THEMES:
      return { ...state };
    case ThemeTypes.FETCH_THEMES_SUCCESS:
      return { ...state, themes: action.payload};
    case ThemeTypes.SUBTHEME:
      return {...state, subTheme: action.payload};
    case ThemeTypes.THEME_ID:
      return {...state, themeId: action.payload}
    default:
      return state;
  }
};
