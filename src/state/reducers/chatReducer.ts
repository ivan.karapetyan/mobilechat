import {ChatAction} from "../action";
import {ChatTypes} from "../actions-types";

interface ChatState {
  data: any[],
  messageKey: string[],
  dialogId: string,
  isQueue: boolean,
  chatStarted: boolean,
}

const initialState: ChatState = {
  data: [],
  messageKey: [],
  dialogId: "",
  isQueue: false,
  chatStarted: false
}

export const ChatReducer = (state = initialState, action: ChatAction): ChatState => {
  switch (action.type) {
    case ChatTypes.SIGN_IN_CHAT:
      return {...state};
    case ChatTypes.WAITING_START_CHAT:
      return {
        ...state,
        isQueue: action.payload.isQueue,
        dialogId: action.payload.dialogId
      }
    case ChatTypes.GET_MESSAGES:
      return {...state};
    case ChatTypes.GET_MESSAGES_SUCCESS:
      return {
        ...state,
        data: action.payload.message,
        messageKey: action.payload.messageKey
      };
    case ChatTypes.SEND_MESSAGES:
      return {...state};
    default:
      return {...state};
  }
}