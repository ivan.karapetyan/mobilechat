import {QueueTypes} from "../actions-types";
import {QueueAction} from "../action";

export interface IState {
  queue: number;
}

const initialState: IState = {
  queue: 0
}

export const QueueReducer = (state = initialState, action: QueueAction): IState => {
  switch (action.type) {
    case QueueTypes.FETCH_QUEUE:
      return {...state}
    case QueueTypes.FETCH_QUEUE_SUCCESS:
      return {...state, queue: action.payload}
    default:
      return {...state}
  }
}