import { combineReducers } from 'redux';
import { ThemeReducer } from './themeReducer';
import {QueueReducer} from "./queueReducer";
import {ChatReducer} from "./chatReducer";

export const rootReducer = combineReducers({
  chat: ChatReducer,
  theme: ThemeReducer,
  queue: QueueReducer,
});

export type RootState = ReturnType<typeof rootReducer>