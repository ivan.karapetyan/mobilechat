import React from 'react';
import {StyleSheet, Pressable, Text, View, TouchableOpacity} from "react-native";
import {useDispatch} from "react-redux";
import {getSubTheme, getThemeId} from "../state/action-creators";

interface themeModal {
  setOpenModal: (arg: boolean) => void;
  themes?: string[];
  subThemes?: any[];
}

const ThemeModal: React.FC<themeModal> = (
  {setOpenModal, themes, subThemes} ) => {
  const dispatch = useDispatch();

  function handleThemeModal(themeId: number): void {
    setOpenModal(false);
    dispatch(getThemeId(themeId));
    dispatch(getSubTheme(""));
  }
  function handleSubThemeModal(subTheme: string): void {
    setOpenModal(false);
    dispatch(getSubTheme(subTheme));
  }
  return (
    <TouchableOpacity
      style={styles.wrapper}
      onPress={() => setOpenModal(false)}
    >
      {themes && <View style={styles.container}>
        {themes?.map((theme, index) => (
          <Pressable
            key={index}
            style={styles.themes__card}
            onPress={() => handleThemeModal(index)}
          >
            <Text style={styles.themes__text}>{theme}</Text>
          </Pressable>
        ))}
      </View>}
      {subThemes && <View style={styles.container}>
        {subThemes?.map((subTheme, index) => (
          <Pressable
            key={index}
            style={styles.themes__card}
            onPress={() => handleSubThemeModal(subTheme)}
          >
            <Text style={styles.themes__text}>{subTheme}</Text>
          </Pressable>
        ))}
      </View>}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: "#007aff",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 50,
  },
  container: {
    height: "100%",
    width: "100%",
    backgroundColor: "#3596ff",
  },
  themes__card: {
    marginHorizontal: 20,
    marginVertical: 40,
    alignItems: "center",
    borderColor: "white"
  },
  themes__text: {
    color: "white",
    fontSize: 32,
  }
})



export default ThemeModal;