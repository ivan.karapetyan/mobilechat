import React, {useEffect} from 'react';
import {Text, View, StyleSheet, Pressable, Image} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {fetchQueue} from "../state/action-creators";
import {RootState} from "../state/reducers";
import useCashData from "../hooks/useCashData";

const Queue: React.FC = () => {
  const dispatch = useDispatch();
  const {queue} = useSelector((state: RootState) => state.queue);
  const { dialogId} = useCashData();

  useEffect(() => {
    if(dialogId) dispatch(fetchQueue(dialogId))
  },[dialogId])

  return (
    <View style={styles.container}>
      <Text style={styles.startPage__text}>Вы в очереди на {queue} месте</Text>
      <Text style={styles.startPage__text}>Вам ответят приблизительно через {queue*3} мин</Text>
      <Image
        style={styles.startPage__loader}
        source={require("../images/623489.png")}
      />
      <Pressable
        style={styles.startPage__button}
        onPress={() => console.log("pressed")}
      >
        <Text style={styles.startPage__button_text}>Напомнить когда придет очередь</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#007aff',
    alignItems: "center",
    paddingHorizontal: 10,
  },
  startPage__text: {
    fontSize: 28,
    color: "white",
    marginTop: 20
  },
  startPage__loader: {
    marginVertical: 50,
    height: "45%",
    resizeMode: "contain",
  },
  startPage__button: {
    width: "80%",
    height: "7.5%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#3596ff",
    borderRadius: 5
  },
  startPage__button_text: {
    fontSize: 18,
    color: "white",
  }
})

export default Queue;