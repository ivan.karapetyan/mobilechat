import React from 'react';
import {TextInput, StyleSheet, View, Text, Button, Pressable, Modal} from 'react-native';
import { useDispatch, useSelector} from 'react-redux';
import {fetchThemes, signInChat} from '../state/action-creators/';
import ThemeModal from "./ThemeModal";
import {RootState} from "../state/reducers";
import {Actions} from "react-native-router-flux";


const StartPage: React.FC = () => {
  const [userName, setUserName] = React.useState<string>("")
  const [themeModal, setThemeModal] = React.useState<boolean> (false);
  const [subThemeModal, setSubThemeModal] = React.useState<boolean> (false);
  const dispatch = useDispatch();
  const {themes, themeId, subTheme} = useSelector((state: RootState) => state.theme);

  React.useEffect( () => {
    dispatch(fetchThemes())
  },[])

  const getSignInChat = (): void => {
    console.log("signInChat======>",userName, subTheme);
    dispatch(signInChat({name: userName, initialMessage: subTheme}))
    Actions.jump("queue")
  }

  return (
    <View style={styles.container}>
      <View style={styles.startPage}>
        <View>
          <Text style={styles.textStyle}>Введите имя: </Text>
          <TextInput
            style={styles.inputStyle}
            onChangeText={setUserName}
            value={userName}
          />
        </View>
        <View>
          <Text style={styles.textStyle}>Выберите тему общения: </Text>
          <Pressable
            style={styles.selectTheme}
            onPress={() => setThemeModal(!themeModal)}
          >
            <Text style={styles.selectTheme__text}>{Object.keys(themes)[themeId]}</Text>
          </Pressable>
        </View>
        <View>
          <Text style={styles.textStyle}>Выберите подтему: </Text>
          <Pressable
            style={styles.selectTheme}
            onPress={() => setSubThemeModal(!subThemeModal)}
          >
            <Text style={styles.selectTheme__text}>{subTheme}</Text>
          </Pressable>
        </View>
        <Button title="Войти в чат" onPress={getSignInChat} />
        <Modal
          visible={themeModal}
          animationType="fade"
        >
          <ThemeModal
            setOpenModal={setThemeModal}
            themes={Object.keys(themes)}
          />
        </Modal>
        <Modal
          visible={subThemeModal}
          animationType="fade"
        >
          <ThemeModal
            setOpenModal={setSubThemeModal}
            subThemes={themes[Object.keys(themes)[themeId]]}
          />
        </Modal>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#007aff',
  },
  startPage: {
    flex: 1,
    margin: 30,
    marginTop: 50,
    justifyContent: 'space-around',
  },
  inputStyle: {
    backgroundColor: 'white',
    marginTop: 20,
    fontSize: 22,
  },
  selectTheme: {
    backgroundColor: 'white',
    height: 45,
    width: "100%",
    marginTop: 10,
  },
  selectTheme__text: {
    color: 'black',
    fontSize: 22,
    margin: 5,
    marginLeft: 15,
  },
  textStyle: {
    color: 'white',
    fontSize: 22,
  }
});


export default StartPage;
