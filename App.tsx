import React from 'react';
import StartPage from './src/components/StartPage';
import {Router, Scene} from "react-native-router-flux";
import Queue from "./src/components/Queue";
import useCashData from "./src/hooks/useCashData";
import ChatPage from "./src/components/ChatPage";


const App: React.FC = () => {
  const {isQueue, chatStarted} = useCashData();
  console.log("from app",chatStarted, isQueue)
  return (
    <Router>
      <Scene key="root" hideNavBar headers>
        <Scene key="startPage" component={StartPage} />
        <Scene key="queue" component={Queue} initial={isQueue}/>
        <Scene key="chatPage" component={ChatPage} initial={chatStarted}/>
      </Scene>
    </Router>
  );
}

export default App;

